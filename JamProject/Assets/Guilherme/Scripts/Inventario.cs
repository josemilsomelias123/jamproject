using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventario : MonoBehaviour
{
    public GameObject[] itens;
    public GameObject[] itensInv;
    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < itens.Length; i++){
            if(itens[i] != null){
                itens[i] = null;
                itensInv[i].GetComponent<Image>().sprite = null;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0)){
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), new Vector2(0, 0));
            if(hit.collider != null){
                if(hit.collider.gameObject.tag == "Item")
                {
                    AddItem(hit.collider.gameObject);
                }else 
                if(hit.collider.gameObject.tag == "Chave3")
                {
                    AddItem(hit.collider.gameObject);
                }else 
                if(hit.collider.gameObject.tag == "Fusivel")
                {
                    AddItem(hit.collider.gameObject);
                }else 
                if(hit.collider.gameObject.tag == "Registro")
                {
                    AddItem(hit.collider.gameObject);
                }
            }
        }
    }

    public bool checkItem(string tagPraChecar){
        for(int i = 0; i < itens.Length; i++){
            if(itens[i].tag == tagPraChecar){
                Debug.Log("Feito");
                return true;
            }
        }
        return false;
    }

    public void AddItem(GameObject item){
        for(int i = 0; i < itens.Length; i++){
            if(itens[i] == null){
                itens[i] = item;
                itensInv[i].GetComponent<Image>().sprite = item.GetComponent<SpriteRenderer>().sprite;
                item.SetActive(false);
                return;
            }
        }
    }
}
