using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RodarObj : MonoBehaviour
{
    public float vel;


    // Update is called once per frame
    void Update()
    {
        if(Input.mouseScrollDelta.y > 0){
            Debug.Log("maior q 0");
        }
        if(Input.mouseScrollDelta.y < 0){
            Debug.Log("menor q 0");
        }
        transform.Rotate(new Vector3(0, 0, 120) * Time.deltaTime * Input.mouseScrollDelta.y * vel);
    }
}
