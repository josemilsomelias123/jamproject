using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemCheck : MonoBehaviour
{
    public string ItemNecessario;
    private GameObject controlador;
    bool ativar;
    public GameObject AlvoObjeto;
    public bool AlvoEstado;
    
    private void Start() {
        controlador = GameObject.FindWithTag("Controlador");
        ativar = false;
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if(other.gameObject.tag == "Player"){
            ativar = controlador.GetComponent<Inventario>().checkItem(ItemNecessario);
            if(ativar == true){
                AlvoObjeto.SetActive(AlvoEstado);
            }
        }
    }
}
