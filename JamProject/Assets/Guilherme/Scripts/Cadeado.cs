using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Cadeado : MonoBehaviour
{
    public GameObject cadeado;
    public int[] senha;
    public int[] valores;
    public GameObject[] textos;
    public string senhaString;
    private string valorString;
    private bool senhaPronta;
    public GameObject painel;
    void Start()
    {
        for(int i = 0; i < 4; i++){
            senha[i] = (int)Random.Range(1, 10);
        }
        senhaString = string.Join("", new List<int>(senha).ConvertAll(i => i.ToString()).ToArray());
        Debug.Log(senhaString);
        cadeado.SetActive(false);
        painel.SetActive(false);
    }

    public void ValorAcrescentar(int pos){
        valores[pos]++;
        if(valores[pos] > 9){
            valores[pos] = 0;
        }
        textos[pos].GetComponent<TextMeshProUGUI>().text = valores[pos].ToString();
    }
    public void ValorDiminuir(int pos){
        valores[pos]--;
        if(valores[pos] < 0){
            valores[pos] = 9;
        }
        textos[pos].GetComponent<TextMeshProUGUI>().text = valores[pos].ToString();
    }

    public void ValorChecar(){
        valorString = string.Join("", new List<int>(valores).ConvertAll(i => i.ToString()).ToArray());
        senhaPronta = senhaString.Equals(valorString);
        if(senhaPronta == true){
            Debug.Log("Sucesso");
            cadeado.SetActive(true);
        }else{
            Debug.Log("Falha");
        }
    }
    /*public void enablePuzzle(){
        if(Input.GetKeyDown(KeyCode.F)){
            cadeado.SetActive(true);
        }
    }*/
}
