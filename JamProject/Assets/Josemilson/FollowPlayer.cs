using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public GameObject player;
    public PlayerController playerController;
    public GameObject gameOver;
    public PATROL1 patrol;

    public LayerMask layer;
    public bool seguePlayer;
    public float dist;

    // Start is called before the first frame update
    void Start()
    {
        seguePlayer = false;
    }

    // Update is called once per frame
    void Update()
    {
         if(seguePlayer == false)
        {
            patrol.SwitchTarget();
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            seguePlayer = true;
            SeguirPlayer();
            
        }        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        seguePlayer = false;
    }

    void SeguirPlayer()
    {
        patrol.targetPos.position = player.transform.position;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            gameOver.SetActive(true);
            playerController.enabled = false;
        }
       
    }
}
