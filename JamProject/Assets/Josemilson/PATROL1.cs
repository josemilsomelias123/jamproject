using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
public class PATROL1 : MonoBehaviour
{
    public Transform[] setoresPosition;
    public bool[] checkSetores;
    public float speed;
    private int target;
    public Transform targetPos;

    public FollowPlayer follow;
    // Start is called before the first frame update
    void Start()
    {
        TargetSelect();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void SwitchTarget()
    {
        
        if(Vector2.Distance(transform.position, targetPos.position) < 2)
        {

            TargetSelect();
            Debug.Log(target);
        }      
    }
    public void TargetSelect()
    {
        target = Random.Range(0,3);
        Debug.Log(target);

        if(target == 0)
        {
            TargetsFalse();
            checkSetores[0] = true;
            targetPos.position = setoresPosition[0].position;
        }
        if (target == 1)
        {
            TargetsFalse();
            checkSetores[1] = true;
            targetPos.position = setoresPosition[1].position;
        }
        if (target == 2)
        {
            TargetsFalse();
            checkSetores[2] = true;
            targetPos.position = setoresPosition[2].position;
        }
        if (target == 3)
        {
            TargetsFalse();
            checkSetores[3] = true;
            targetPos.position = setoresPosition[3].position;
        }
    }
    
    void TargetsFalse()
    {
        checkSetores[0] = false;
        checkSetores[1] = false;
        checkSetores[2] = false;
        checkSetores[3] = false;
    }
}
