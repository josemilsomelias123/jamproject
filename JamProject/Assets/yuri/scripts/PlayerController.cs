using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject seletor;
    
    private Rigidbody2D playerRigidbody2D;
    private Vector2 point;
    //private Animator animator;
    //public float veloci;

    void Start()
    {
        playerRigidbody2D = GetComponent<Rigidbody2D>();
        point = transform.position;
        //animator = GetComponent<Animator>();
        
    }

    void Update()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (Input.GetMouseButtonDown(0))
        {
            point = new Vector2(mousePos.x, mousePos.y);
            //seletor.transform.position = point + Vector3(0, 0.25f, 0);
            seletor.SetActive(true);
            //Instantiate(seletor);
            seletor.transform.position = point;
            //seletor.SetActive(false);
        }else if(Vector3.Distance(gameObject.transform.position, seletor.transform.position) < 0.5 && Vector3.Distance(gameObject.transform.position, seletor.transform.position) > -0.5)
        {
            seletor.SetActive(false);
        }
        
        //playerRigidbody2D.MovePosition(playerRigidbody2D.position + point  * Time.fixedDeltaTime);
       // animator.SetBool("Horizontal", true);
        //animator.SetBool("Vertical", false);
        //animator.SetFloat("Speed", movement.magnitude);


        transform.position = Vector2.MoveTowards(transform.position, point , Time.fixedDeltaTime );

        
    }
    
    void OnTriggerEnter2D(Collider2D stop) 
    {
        if (stop.tag == "Wall")
        {
            point = transform.position;

            //Debug.log("colidiu");
        }

    }

    
}
