using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TP : MonoBehaviour
{
    public GameObject destino;

    void OnTriggerEnter2D(Collider2D stop) 
    {
        if (stop.tag == "Player")
        {
           stop.gameObject.transform.position = destino.transform.position;
        }

    }


}
