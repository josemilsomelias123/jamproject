using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuzController : MonoBehaviour
{

    SpriteRenderer imagem;

    private Animator animator;

    public PlayerController PlayerController;

    public GameObject teste;


    void Start()
    {
        imagem = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        Vector3 mousePos = Input.mousePosition;
        Vector3 screenPoint = Camera.main.WorldToScreenPoint(transform.position);

        Vector2 offset = new Vector2(mousePos.x - screenPoint.x, mousePos.y - screenPoint.y);

        float angle = Mathf.Atan2(offset.y, offset.x) * Mathf.Rad2Deg;

        transform.rotation = Quaternion.Euler(0, 0, angle);

        imagem.flipY = (mousePos.x < screenPoint.x);


        if(Vector3.Distance(gameObject.transform.position, teste.transform.position) < 0.5 && Vector3.Distance(gameObject.transform.position, teste.transform.position) > -0.5)
        {
            animator.SetBool("andando", true);

        }else
        {
            animator.SetBool("andando", false);
        }
        

    }
}
